<?php

namespace nkovacs\errbit;

use yii\helpers\ArrayHelper;

/**
 * @deprecated
 */
class WebErrorHandler extends \credy\errbit\WebErrorHandler
{
    public function init()
    {
        if (!$this->errbitApiKey) {
            $this->errbitApiKey = ArrayHelper::remove($this->errbit, 'api_key');
        }
        if (!$this->errbitHost) {
            $this->errbitHost = ArrayHelper::remove($this->errbit, 'host');
        }

        parent::init();
    }
}
