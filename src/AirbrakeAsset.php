<?php

namespace credy\errbit;

use yii\web\AssetBundle;
use yii\web\View;

class AirbrakeAsset extends AssetBundle
{
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    public $sourcePath = '@npm/airbrake--browser/umd';

    public $js = [
        'airbrake.js'
    ];
}
