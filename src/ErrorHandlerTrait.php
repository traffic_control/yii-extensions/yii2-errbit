<?php

namespace credy\errbit;

use Errbit\Errbit;
use Errbit\Writer\SocketWriter;
use Errbit\Writer\WriterInterface;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\Instance;

/**
 * ErrorHandlerTrait should be attached to an error handler.
 * It sends errors to errbit.
 */
trait ErrorHandlerTrait
{
    /**
     * @var string errbit api key
     */
    public $errbitApiKey;

    /**
     * @var string errbit host
     */
    public $errbitHost;

    /**
     * @var array errbit configuration
     */
    public $errbit = [];

    /**
     * @var WriterInterface|string|array
     */
    public $writer = SocketWriter::class;

    /**
     * @var Errbit
     */
    private $_instance;

    public function init()
    {
        if ($this->errbitApiKey === null) {
            throw new InvalidConfigException('Errbit API key is required.');
        }
        if ($this->errbitHost === null) {
            throw new InvalidConfigException('Errbit host is required.');
        }

        parent::init();
    }

    public function register()
    {
        $config = [
            'api_key' => $this->errbitApiKey,
            'host'    => $this->errbitHost,
        ];

        if (is_array($this->errbit)) {
            $this->errbit = array_merge($config, $this->errbit);
        }

        $this->_instance = new Errbit($this->errbit);

        $writer = Instance::ensure($this->writer, WriterInterface::class);

        $this->_instance->setWriter($writer);

        parent::register();
    }

    public function logException($exception)
    {
        if ($this->_instance) {
            $opts = [];
            $controller = null;
            if (Yii::$app) {
                $controller = Yii::$app->controller;
            }
            if ($controller !== null) {
                $opts['controller'] = $controller->uniqueId;
                if ($controller->action !== null) {
                    $opts['action'] = $controller->action->id;
                }
                if ($controller instanceof UserInfoInterface) {
                    $opts['user'] = $controller->getErrbitUserInfo();
                }
            }
            $this->_instance->notify($exception, $opts);
        }
        parent::logException($exception);
    }
}
