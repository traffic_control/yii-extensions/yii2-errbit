<?php

namespace credy\errbit;

use Yii;
use yii\base\ActionEvent;
use yii\base\Application;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Application as WebApplication;
use yii\web\View;

/**
 * WebErrorHandler sends errors to errbit.
 */
class WebErrorHandler extends \yii\web\ErrorHandler
{
    use ErrorHandlerTrait {
        ErrorHandlerTrait::register as traitRegister;
    }

    /**
     * @var boolean whether to enable javascript error notifier
     */
    public $jsNotifier = false;

    /**
     * @var array additional js options
     * @see https://www.npmjs.com/package/@airbrake/browser
     */
    public $jsOptions = [];

    public function register()
    {
        $app = Yii::$app;

        if ($this->jsNotifier && $app instanceof WebApplication) {
            $app->on(Application::EVENT_BEFORE_REQUEST, function () use ($app) {
                $app->view->registerAssetBundle(AirbrakeAsset::class);

                $app->view->registerJs($this->getJavascript(), View::POS_HEAD);

                $app->on(Application::EVENT_BEFORE_ACTION, function (ActionEvent $event) use ($app) {
                    if ($event->action->controller instanceof UserInfoInterface) {
                        $app->view->registerJs($this->getUserInfoJavascript($event->action->controller), View::POS_HEAD);
                    }
                });
            });
        }
        $this->traitRegister();
        parent::register();
    }

    protected function getJavascript()
    {
        $host = $this->errbit['host'];
        if (Url::isRelative($host)) {
            $host = '//' . $host;
        }

        $options = [
            'projectId'   => 1,
            'projectKey'  => $this->errbit['api_key'],
            'host'        => $host,
            'environment' => $this->errbit['environment_name'] ?? null
        ];

        $options = ArrayHelper::merge($this->jsOptions, $options);

        $options = Json::htmlEncode($options);

        return <<<JAVASCRIPT
            (function (document,window,undefined) {
                window.airbrake = new Airbrake.Notifier({$options});
            })(document,window);
JAVASCRIPT
        ;
    }

    protected function getUserInfoJavascript(UserInfoInterface $userInfoObject)
    {
        $userInfo = Json::htmlEncode($userInfoObject->getErrbitUserInfo());

        return <<<JAVASCRIPT
            (function (document,window,undefined) {
                window.airbrake.addFilter(function (notice) {
                    notice.session['user-attributes']={$userInfo};
                    return notice;
                });
            })(document,window);
JAVASCRIPT
        ;
    }
}
