<?php

namespace tests\unit;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use credy\errbit\ConsoleErrorHandler;
use credy\errbit\UserInfoInterface;
use Errbit\Writer\WriterInterface;
use Exception;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\console\Application;
use yii\console\Controller;

class ConsoleErrorHandlerTest extends Unit
{
    public function testRegisterNoApiKey()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Errbit API key is required.');

        $errorHandler = new ConsoleErrorHandler([
            'errbitHost' => 'test.example.org',
        ]);
    }

    public function testRegisterNoHost()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Errbit host is required.');

        $errorHandler = new ConsoleErrorHandler([
            'errbitApiKey' => 'apikey',
        ]);
    }

    public function testSuccessfulRegistration()
    {
        new ConsoleErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ]);
    }

    public function testLogException()
    {
        $handler = new ConsoleErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'writer' => $this->makeEmpty(WriterInterface::class, [
                'write' => Expected::once(function (Exception $exception, $opts) {
                    $this->assertEquals('test exception', $exception->getMessage());
                    $this->assertArrayNotHasKey('controller', $opts);
                    $this->assertArrayNotHasKey('action', $opts);
                    $this->assertArrayNotHasKey('user', $opts);
                })
            ])
        ]);

        $handler->register();

        $handler->logException(new Exception('test exception'));
    }

    public function testLogExceptionWithApplicationSetButWeDoNotHaveControllerYet()
    {
        $handler = new ConsoleErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'writer' => $this->makeEmpty(WriterInterface::class, [
                'write' => Expected::once(function (Exception $exception, $opts) {
                    $this->assertEquals('test exception', $exception->getMessage());
                    $this->assertArrayNotHasKey('controller', $opts);
                    $this->assertArrayNotHasKey('action', $opts);
                    $this->assertArrayNotHasKey('user', $opts);
                })
            ])
        ]);

        $handler->register();

        Yii::$app = new Application([
            'id' => 'test',
            'basePath' => __DIR__
        ]);

        $handler->logException(new Exception('test exception'));

        Yii::$app = null;
    }

    public function testLogExceptionWithControllerSetButWeDoNotHaveActionYet()
    {
        $handler = new ConsoleErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'writer' => $this->makeEmpty(WriterInterface::class, [
                'write' => Expected::once(function (Exception $exception, $opts) {
                    $this->assertEquals('test exception', $exception->getMessage());
                    $this->assertArrayNotHasKey('action', $opts);
                    $this->assertArrayNotHasKey('user', $opts);
                    $this->assertArrayHasKey('controller', $opts);
                    $this->assertEquals('controller', $opts['controller']);
                })
            ])
        ]);

        $handler->register();

        Yii::$app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        Yii::$app->controller = new Controller('controller', Yii::$app);

        $handler->logException(new Exception('test exception'));

        Yii::$app = null;
    }

    public function testLogExceptionWithActionSet()
    {
        $handler = new ConsoleErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'writer' => $this->makeEmpty(WriterInterface::class, [
                'write' => Expected::once(function (Exception $exception, $opts) {
                    $this->assertEquals('test exception', $exception->getMessage());
                    $this->assertArrayNotHasKey('user', $opts);

                    $this->assertArrayHasKey('controller', $opts);
                    $this->assertEquals('controller', $opts['controller']);

                    $this->assertArrayHasKey('action', $opts);
                    $this->assertEquals('action', $opts['action']);
                })
            ])
        ]);

        $handler->register();

        Yii::$app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        Yii::$app->controller = new Controller('controller', Yii::$app);

        Yii::$app->controller->action = new Action('action', Yii::$app->controller);

        $handler->logException(new Exception('test exception'));

        Yii::$app = null;
    }

    public function testLogExceptionWithUserInfoSet()
    {
        $handler = new ConsoleErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'writer' => $this->makeEmpty(WriterInterface::class, [
                'write' => Expected::once(function (Exception $exception, $opts) {
                    $this->assertEquals('test exception', $exception->getMessage());
                    $this->assertArrayHasKey('user', $opts);
                    $this->assertEquals([
                        'testuser' => 'test user'
                    ], $opts['user']);

                    $this->assertArrayHasKey('controller', $opts);
                    $this->assertEquals('controller', $opts['controller']);

                    $this->assertArrayHasKey('action', $opts);
                    $this->assertEquals('action', $opts['action']);
                })
            ])
        ]);

        $handler->register();

        Yii::$app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        Yii::$app->controller = new class ('controller', Yii::$app) extends Controller implements UserInfoInterface {
            public function getErrbitUserInfo()
            {
                return [
                    'testuser' => 'test user'
                ];
            }
        };

        Yii::$app->controller->action = new Action('action', Yii::$app->controller);

        $handler->logException(new Exception('test exception'));

        Yii::$app = null;
    }
}
