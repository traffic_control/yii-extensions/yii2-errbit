<?php

namespace tests\unit;

use Codeception\Test\Unit;
use nkovacs\errbit\WebErrorHandler;

class NkovacsWebErrorHandlerTest extends Unit
{
    public function testExtractFromArray()
    {
        $errorHandler = new WebErrorHandler([
            'errbit' => [
                'host' => 'test.example.org',
                'api_key' => 'api_key',
            ]
        ]);

        $this->assertEquals('test.example.org', $errorHandler->errbitHost);
        $this->assertEquals('api_key', $errorHandler->errbitApiKey);
    }
}
