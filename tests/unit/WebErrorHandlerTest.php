<?php

namespace tests\unit;

use Closure;
use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use credy\errbit\AirbrakeAsset;
use credy\errbit\UserInfoInterface;
use credy\errbit\WebErrorHandler;
use tests\MultiCallable;
use yii\base\Action;
use yii\base\ActionEvent;
use yii\base\Controller;
use yii\base\InvalidConfigException;
use yii\web\Application;
use yii\web\View;

class WebErrorHandlerTest extends Unit
{
    public function testRegisterNoApiKey()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Errbit API key is required.');

        $errorHandler = new WebErrorHandler([
            'errbitHost' => 'test.example.org',
        ]);
    }

    public function testRegisterNoHost()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Errbit host is required.');

        $errorHandler = new WebErrorHandler([
            'errbitApiKey' => 'apikey',
        ]);
    }

    public function testSuccessfulRegistration()
    {
        new WebErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ]);
    }

    public function testNoViewFileRegistered()
    {
        $app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        $viewComponent = $this->make(View::class, [
            'registerAssetBundle' => Expected::never(),
            'registerJs' => Expected::never(),
        ]);

        $app->setComponents([
            'view' => $viewComponent
        ]);

        $errorHandler = new WebErrorHandler([
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ]);

        $errorHandler->register();
        $app->trigger(Application::EVENT_BEFORE_REQUEST);
    }

    public function testViewFileRegistered()
    {
        $app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        $viewComponent = $this->make(View::class, [
            'registerAssetBundle' => Expected::once(function ($assetBundle) {
                $this->assertEquals(AirbrakeAsset::class, $assetBundle);
            }),
            'registerJs' => Expected::once(function ($javascript) {
                $this->assertStringContainsString('window.airbrake = new Airbrake.Notifier(' .
                '{"projectId":1,"projectKey":"apikey","host":"\\/\\/test.example.org","environment":null}' .
                ');', $javascript);
            }),
        ]);

        $app->setComponents([
            'view' => $viewComponent
        ]);

        $errorHandler = new WebErrorHandler([
            'jsNotifier' => true,
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ]);

        $errorHandler->register();
        $app->trigger(Application::EVENT_BEFORE_REQUEST);
    }

    public function testJsOptions()
    {
        $app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        $viewComponent = $this->make(View::class, [
            'registerAssetBundle' => Expected::once(function ($assetBundle) {
                $this->assertEquals(AirbrakeAsset::class, $assetBundle);
            }),
            'registerJs' => Expected::once(function ($javascript) {
                $this->assertStringContainsString('window.airbrake = new Airbrake.Notifier(' .
                '{"optionName":{"key":"value"},"projectId":1,"projectKey":"apikey","host":"\/\/test.example.org","environment":null}' .
                ');', $javascript);
            }),
        ]);

        $app->setComponents([
            'view' => $viewComponent
        ]);

        $errorHandler = new WebErrorHandler([
            'jsNotifier' => true,
            'jsOptions' => [
                'optionName' => ['key' => 'value'],
            ],
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ]);

        $errorHandler->register();
        $app->trigger(Application::EVENT_BEFORE_REQUEST);
    }


    public function testEnvironmentNamePassed()
    {
        $app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        $viewComponent = $this->make(View::class, [
            'registerAssetBundle' => Expected::once(function ($assetBundle) {
                $this->assertEquals(AirbrakeAsset::class, $assetBundle);
            }),
            'registerJs' => Expected::once(function ($javascript) {
                $this->assertStringContainsString('window.airbrake = new Airbrake.Notifier(' .
                '{"projectId":1,"projectKey":"apikey","host":"\\/\\/test.example.org","environment":"environment"}' .
                ');', $javascript);
            }),
        ]);

        $app->setComponents([
            'view' => $viewComponent
        ]);

        $errorHandler = new WebErrorHandler([
            'jsNotifier' => true,
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'errbit' => [
                'environment_name' => 'environment'
            ]
        ]);
        $errorHandler->register();
        $app->trigger(Application::EVENT_BEFORE_REQUEST);
    }

    public function testActionCalledControllerNotUserInfoInterface()
    {
        $app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        $viewComponent = $this->make(View::class, [
            'registerAssetBundle' => Expected::once(),
            'registerJs' => Expected::once(),
        ]);

        $app->setComponents([
            'view' => $viewComponent
        ]);

        $errorHandler = new WebErrorHandler([
            'jsNotifier' => true,
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ]);

        $errorHandler->register();
        $app->trigger(Application::EVENT_BEFORE_REQUEST);
        $app->trigger(Application::EVENT_BEFORE_ACTION, new ActionEvent(new Action('test_action', new Controller('test_controller', $app))));
    }

    public function testActionCalled()
    {
        $app = new Application([
            'id' => 'test',
            'basePath' => __DIR__,
        ]);

        $viewComponent = $this->make(View::class, [
            'registerAssetBundle' => Expected::once(),
            'registerJs' => Expected::exactly(2, Closure::fromCallable(new MultiCallable([
                function () {
                },
                function ($javascript) {
                    $this->assertStringContainsString('notice.session[\'user-attributes\']={"id":"user_id"};', $javascript);
                },
            ]))),
        ]);

        $app->setComponents([
            'view' => $viewComponent
        ]);

        $errorHandler = new WebErrorHandler([
            'jsNotifier' => true,
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ]);

        $errorHandler->register();
        $app->trigger(Application::EVENT_BEFORE_REQUEST);
        $app->trigger(Application::EVENT_BEFORE_ACTION, new ActionEvent(
            new Action(
                'test_action',
                new class ('controller_class', $app) extends Controller implements UserInfoInterface
                {
                    public function getErrbitUserInfo()
                    {
                        return [
                            'id' => 'user_id'
                        ];
                    }
                }
            )
        ));
    }
}
