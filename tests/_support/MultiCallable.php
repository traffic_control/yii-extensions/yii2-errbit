<?php

namespace tests;

class MultiCallable
{
    private $_methods;

    public function __construct($methods)
    {
        $this->_methods = $methods;
    }

    public function __invoke(...$args)
    {
        $method = array_shift($this->_methods);

        return call_user_func_array($method, $args);
    }
}
