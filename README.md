Yii 2 Errbit error handler
==========================
Logs errors to [errbit](https://github.com/errbit/errbit)

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist credy/yii2-errbit "*"
```

or add

```
"credy/yii2-errbit": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Replace the default error handler with either `\credy\errbit\WebErrorHandler` or `\credy\errbit\ConsoleErrorHandler`:

```php
...
    'components' => [
        'errorHandler' => [
            'class' => credy\errbit\ConsoleErrorHandler::className(),
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ],
    ],
...
```

or

```php
...
    'components' => [
        'errorHandler' => [
            'class' => credy\errbit\ConsoleErrorHandler::className(),
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
        ],
    ],
...
```

You can pass additional options to errbitPHP:

```php
...
    'components' => [
        'errorHandler' => [
            'class' => credy\errbit\ConsoleErrorHandler::className(),
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'errbit' => [
                'environment_name' => 'development',
            ],
        ],
    ],
...
```

To enable the js notifier:

```php
...
    'components' => [
        'errorHandler' => [
            'class' => credy\errbit\WebErrorHandler::className(),
            'errbitApiKey' => 'apikey',
            'errbitHost' => 'test.example.org',
            'jsNotifier' => true,
        ],
    ],
...
```

You can pass additional options to the js plugin using the `jsOptions` property.

If the controller implements `UserInfoInterface`, the information returned by `getErrbitUserInfo` will also be
sent to errbit.
